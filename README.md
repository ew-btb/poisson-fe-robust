Evan Wright

This project contains an R package which computes standard errors
following Wooldridge (1999) for Poisson regression with
fixed effects, and a hypothesis test of the conditional mean
assumption (3.1).

Can be built with
library(devtools)
build(pkg = "poisFErobust")

Available on CRAN:
https://cran.r-project.org/package=poisFErobust

Wooldridge, Jeffrey M. (1999): "Distribution-free estimation of some nonlinear
    panel data models," Journal of Econometrics, 90, 77-97.