2.0.0 - 2020-02-17
Release v2.0.0 on CRAN.
Deprecated:
- index.name argument (was not used)

Added:
- Additional tests for missing data and group.name is factor
- Performance improvements by indexing with RLE

0.0.2 - 2016-11-09
Now a clean general case script exists with example data.
Added:
- example-data.r
- main-script.r

Updated:
- citation, license, readme
- requirements no longer includes doParallel R package

Removed:
- initial-script.r

0.0.1 - 2016-11-07
Added:
- Initial project files
- CITATION.txt
- LICENSE.txt (MIT)
- README.txt
- requirements.txt
- This changelog.txt