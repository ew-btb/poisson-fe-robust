## Version 2.0.0 submission
Major version incremented because one function argument is now deprecated.
It was not actually used in the previous versions, but some existing code
could fail. There are no reverse dependencies.

## Test environments
* Windows 10, R 3.5.1 (64-bit)
* winbuilder, R 3.6.2 (2019-12-12)
* R-hub builder:
  - Windows Server 2008 R2 SP1, R-devel, 32/64 bit
  - Ubuntu Linux 16.04 LTS, R-release, GCC
  - Fedora Linux, R-devel, clang, gfortran

## R CMD check results
0 errors | 0 warnings | 1 notes

```
* checking CRAN incoming feasibility ... NOTE
Maintainer: ‘Evan Wright <enwright@umich.edu>’

Possibly mis-spelled words in DESCRIPTION:
  Wooldridge (9:23)
```

It's spelled correctly.



## Reverse depends
There are no reverse dependencies.