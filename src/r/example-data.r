# This script creates example data.
library(data.table)

# ex.dt.good satisfies the conditional mean assumption (3.1) in
# Wooldridge (1999).
set.seed(1)
ex.dt.good <- data.table(id = as.factor(rep(1:50, each = 10)),
                         day = rep(1:10, 50),
                         fe = rep(rnorm(50, mean = 0, sd = 1), each = 10),
                         x1 = rnorm(500, mean = 0, sd = 1),
                         x2 = rnorm(500, mean = 0, sd = 1))
ex.dt.good[, y := rpois(1, exp(fe + x1 + x2)), by = 1:nrow(ex.dt.good)]

# ex.dt.bad violates the conditional mean assumption (3.1) in
# Wooldridge (1999). y depends on future values of x1.
ex.dt.bad <- copy(ex.dt.good)
ex.dt.bad[, x1.lead := shift(x1, type="lead"), by = id]
ex.dt.bad <- ex.dt.bad[!is.na(x1.lead)]
ex.dt.bad[, y := rpois(1, exp(fe + x1 + x2 + 2.5*x1.lead)),
           by = 1:nrow(ex.dt.bad)]


# # Test example data
# library(glmmML)
# test.fit <- glmmboot(y ~ x1 + x2, data = ex.dt.bad, 
#                      family = "poisson", cluster = id,
#                      boot = 0)
# 
# xvars.test <- c("x1", "x2")
# 
# poiscoef.test <- coef(test.fit)
# 
# p.test <- length(poiscoef.test)
# 
# 
# # begin -------------------------------------------------------------------
# all.ids.test <- unique(ex.dt.bad$id)
# 
# mylist.names.test <- as.character(all.ids.test)
# outlist.test <- vector("list", length(mylist.names.test))
# 
# bigN <- length(all.ids.test)
# for(i.index in 1:bigN) {
#   i <- all.ids.test[i.index]
#   this.id <- ex.dt.bad[id == i,]
#   listtoreturn <- list("n_i" = NULL, "p_i" = NULL, "W_i" = NULL, "L_i" = NULL, "u_i" = NULL)
#   n_i <- sum(this.id$y)
#   xvarcolmatrix <- as.matrix(this.id[, .SD, .SDcols = xvars.test])
#   sumfor_it <- sum(exp(xvarcolmatrix %*% poiscoef.test))
#   p_i <- exp(xvarcolmatrix %*% poiscoef.test)/
#     sumfor_it
#   outlam <- matrix(0, length(this.id$day), p.test) #TxP
#   for (tday in 1:length(this.id$day)){
#     this.t.x <- as.matrix(this.id[tday, .SD, .SDcols = xvars.test]) #1xP
#     exbeta <- exp(this.t.x %*% poiscoef.test)
#     for (pind in 1:p.test){
#       outlam[tday, pind] <- (this.t.x[pind] * exbeta * sumfor_it - 
#                                sum(xvarcolmatrix[,pind] *
#                                      exp(xvarcolmatrix %*% poiscoef.test)) * exbeta)/(sumfor_it^2)
#       #       outlam[tday, pind] <- this.t.x[pind]*exbeta/sumfor_it - 
#       #         (exbeta / (sumfor_it^2)) * 
#     }
#   }
#   listtoreturn$"L_i" <- outlam
#   listtoreturn$"W_i" <- solve(diag(as.vector(p_i)))
#   listtoreturn$"u_i" <- as.matrix(this.id[, y]) - n_i * p_i
#   listtoreturn$"n_i" <- n_i
#   listtoreturn$"p_i" <- p_i
#   outlist.test[[i]] <- listtoreturn
# }
# 
# 
# bigK <- matrix(0, p.test, p.test)
# for (i in 1:bigN){
#   bigK <- bigK + 1/bigN * (t(outlist.test[[i]]$L_i)) %*% (outlist.test[[i]]$n_i * outlist.test[[i]]$L_i)
# }
# 
# bigA <- matrix(0, p.test, p.test)
# for (i in 1:bigN){
#   bigA <- bigA + 1/bigN * outlist.test[[i]]$n_i * (t(outlist.test[[i]]$L_i) %*% outlist.test[[i]]$W_i %*%
#                                                 outlist.test[[i]]$L_i)
# }
# 
# bigB <- matrix(0, p.test, p.test)
# for (i in 1:bigN){
#   bigB <- bigB + 1/bigN * t(outlist.test[[i]]$L_i) %*% outlist.test[[i]]$W_i %*% 
#     outlist.test[[i]]$u_i %*% t(outlist.test[[i]]$u_i) %*% outlist.test[[i]]$W_i %*% 
#     outlist.test[[i]]$L_i
# }
# 
# rmat <- matrix(0, bigN, p.test)
# for (i in 1:bigN){
#   rmat[i,] <- t(outlist.test[[i]]$u_i) %*% (outlist.test[[i]]$L_i - outlist.test[[i]]$W_i %*% 
#                                          outlist.test[[i]]$L_i %*% solve(bigA) %*% t(bigK))
# }
# 
# rmat.df <- as.data.frame(rmat)
# rmat.df$ones <- rep(1, nrow(rmat.df))
# lmfit <- lm(ones ~ -1 + ., data = rmat.df[1:bigN,])
# ssr <- sum(lmfit$residuals^2)
# teststat <- bigN - ssr
# 1 - pchisq(q = teststat, df = p.test)
# 
# # Robust SE from Wooldridge (1999)
# se.rob <- sqrt(diag((solve(bigA) %*% bigB %*% solve(bigA))/ bigN))
# se.rob
